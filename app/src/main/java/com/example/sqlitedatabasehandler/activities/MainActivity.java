package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.adapter.MyAdapter;
import com.example.sqlitedatabasehandler.databaseHandling.ProductHandler;
import com.example.sqlitedatabasehandler.objects.ProductUi;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton login;
    TextView nameProduct, priceProduct, categoryProduct, statusProduct, detailProduct;
    ListView listViewProduct;
    ArrayList<ProductUi> arrayList;
    MyAdapter adapter;
    Intent intent;
    ProductHandler productHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameProduct = (TextView) findViewById(R.id.name_product_home);
        priceProduct = (TextView) findViewById(R.id.price_product_home);
        categoryProduct = (TextView) findViewById(R.id.category_product_home);
        statusProduct = (TextView) findViewById(R.id.status_product_home);
        detailProduct = (TextView) findViewById(R.id.detail_product_home);
        listViewProduct = (ListView) findViewById(R.id.list_item_product_home);
        productHandler = new ProductHandler(this);
        arrayList = new ArrayList<>();
        loadDataInListView();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        login = (ImageButton) findViewById(R.id.button_login_home);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadDataInListView() {
        arrayList = productHandler.getAllData();
        adapter = new MyAdapter(this, arrayList);
        listViewProduct.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}