package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;

import java.io.ByteArrayOutputStream;

public class choose_profile_picture extends AppCompatActivity {

    ImageButton profile1, profile2, profile3, profile4, profile5, profile6;
    Intent intent;
    String prefs = LoginActivity.MyPREFERENCES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_profile_picture);
        UserHandler userHandler = new UserHandler(choose_profile_picture.this);
        // Get username from shared  prefs
        SharedPreferences sharedPreferences = getSharedPreferences(prefs, Context.MODE_PRIVATE);
        String username = (sharedPreferences.getString(LoginActivity.usernamePref, ""));
        profile1 = (ImageButton) findViewById(R.id.profile_picture_1);
        profile1.setImageResource(R.drawable.profile_man1);
        profile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_man1);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        profile2 = (ImageButton) findViewById(R.id.profile_picture_2);
        profile2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_man2);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        profile3 = (ImageButton) findViewById(R.id.profile_picture_3);
        profile3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_man3);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        profile4 = (ImageButton) findViewById(R.id.profile_picture_4);
        profile4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_woman1);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        profile5 = (ImageButton) findViewById(R.id.profile_picture_5);
        profile5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_woman2);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        profile6 = (ImageButton) findViewById(R.id.profile_picture_6);
        profile6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Turn image into byte array
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.profile_woman3);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] img = byteArrayOutputStream.toByteArray();
                // Upload image to database
                UserHandler userHandler = new UserHandler(choose_profile_picture.this);
                userHandler.updateImage(username, img);
                finish();
                intent = new Intent(choose_profile_picture.this, AccountActivity.class);
                startActivity(intent);
            }
        });
    }
}