package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.content.SharedPreferences;
import android.widget.TextView;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;
import com.example.sqlitedatabasehandler.databinding.ActivityAccountBinding;

public class AccountActivity extends AppCompatActivity {
    private ActivityAccountBinding binding;
    ImageView profilePic;
    Bitmap profileBitmap;
    TextView FLName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAccountBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.accountSeeAdsButton.setOnClickListener(v -> navigateToUserAds());
        binding.accountPublishAdsButton.setOnClickListener(v -> navigateToPublishAds());
        binding.accountModifyAccountButton.setOnClickListener(v -> navigateToModifyAccount());

        // Get stored username to load corresponding profile picture
        SharedPreferences sharedPreferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        String username = (sharedPreferences.getString(LoginActivity.usernamePref, ""));
        UserHandler userHandler = new UserHandler(AccountActivity.this);
        profilePic = (ImageView) findViewById(R.id.profile_picture);
        profileBitmap = userHandler.getImage(username);
        if (profileBitmap != null) {
            profilePic.setImageBitmap(profileBitmap);
        }
        // Get user first and last name to show in textview
        String firstName = userHandler.getUserFirstName(username);
        String lastName = userHandler.getUserLastName(username);
        String firstLastName = firstName + " " + lastName;
        FLName = (TextView) findViewById(R.id.first_name_last_name);
        FLName.setText(firstLastName);
    }

    private void navigateToModifyAccount() {
        Intent intent = new Intent(AccountActivity.this, InfoAccountActivity.class);
        startActivity(intent);
    }

    private void navigateToPublishAds() {
        Intent intent = new Intent(this, PublishAdsActivity.class);
        startActivity(intent);
    }

    private void navigateToUserAds() {
        Intent intent = new Intent(this, MyProductsActivity.class);
        startActivity(intent);
    }
}