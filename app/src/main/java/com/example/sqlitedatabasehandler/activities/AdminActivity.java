package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.sqlitedatabasehandler.R;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
    }
}