package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.sqlitedatabasehandler.databaseHandling.ProductHandler;
import com.example.sqlitedatabasehandler.databinding.ActivityPublishAdsBinding;
import com.example.sqlitedatabasehandler.objects.Product;
import com.example.sqlitedatabasehandler.objects.UserContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PublishAdsActivity extends AppCompatActivity {
    private ActivityPublishAdsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPublishAdsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.publishCancelButton.setOnClickListener(view -> handleCancelButton());
        binding.publishAdsButton.setOnClickListener(view -> handleSaveButton());
        binding.publishPictureUpload.setOnClickListener(view -> handlePictureUpload());
    }

    // Picture upload feature for products... Not enough time to implement
    private void handlePictureUpload() {

    }

    private void handleSaveButton() {
        String publishName = binding.publishNameInput.getText().toString();
        int publishPrice = Integer.parseInt(binding.publishPriceInput.getText().toString());
        int quantityProduct = Integer.parseInt(binding.publishQuantityInput.getText().toString());
        String detailProduct = binding.publishDetailInput.getText().toString();
        int categoryId = binding.publishCategorySelect.getSelectedItemPosition() + 1;
        int statusId = binding.publishStatusSelect.getSelectedItemPosition() + 1;
        int display = binding.publishDiplayButtonRadio.isChecked() ? 1 : 0;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
        ProductHandler productHandler = new ProductHandler(this);
        Product product = new Product(publishName,
                publishPrice,
                quantityProduct,
                detailProduct,
                "",
                display,
                sdf.format(new Date()),
                categoryId,
                statusId,
                UserContext.LOGGED_USER_ID);
        productHandler.addProduct(product);
        finish();
    }

    private void handleCancelButton(){
        finish();
    }
}