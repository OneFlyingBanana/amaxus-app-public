package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;

public class InfoAccountActivity extends AppCompatActivity {
    EditText firstName, lastName, email, phoneNbr, street, npa, city, username, password;
    Button addProfilePic, cancel, save;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_info);
        firstName = (EditText) findViewById(R.id.account_info_first_name);
        lastName = (EditText) findViewById(R.id.account_info_second_name);
        email = (EditText) findViewById(R.id.account_info_mail);
        phoneNbr = (EditText) findViewById(R.id.account_info_phone);
        street = (EditText) findViewById(R.id.account_info_street);
        npa = (EditText) findViewById(R.id.account_info_npa);
        city = (EditText) findViewById(R.id.account_info_city);
        username = (EditText) findViewById(R.id.account_info_username);
        password = (EditText) findViewById(R.id.account_info_password);
        addProfilePic = (Button) findViewById(R.id.info_account_upload_profile);
        addProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                intent = new Intent(InfoAccountActivity.this, choose_profile_picture.class);
                startActivity(intent);
            }
        });
        save = (Button) findViewById(R.id.info_account_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserHandler userHandler = new UserHandler(InfoAccountActivity.this);
                String fName = firstName.getText().toString();
                String lName = lastName.getText().toString();
                String eMail = email.getText().toString();
                String phoneNum = phoneNbr.getText().toString();
                String strt = street.getText().toString();
                String uName = username.getText().toString();
                String pWord = password.getText().toString();
                // Check if all required fields are filled
                if (fName.isEmpty() || lName.isEmpty() || eMail.isEmpty() || uName.isEmpty() || pWord.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Veuillez compléter votre nom, prénom, mail, nom d\'utilisateur et mot de passe.", Toast.LENGTH_LONG).show();
                } else {
                    userHandler.updateUser(fName, lName, eMail, phoneNum, strt, uName, pWord);
                    Toast.makeText(getApplicationContext(), "Mise à jour réussie", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancel = (Button) findViewById(R.id.info_account_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}