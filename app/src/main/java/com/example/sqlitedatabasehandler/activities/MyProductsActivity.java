package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import com.example.sqlitedatabasehandler.adapter.MyProductAdapter;
import com.example.sqlitedatabasehandler.databaseHandling.ProductHandler;
import com.example.sqlitedatabasehandler.databinding.ActivityMyProductBinding;
import com.example.sqlitedatabasehandler.objects.UserContext;

public class MyProductsActivity extends AppCompatActivity {
    private MyProductAdapter adapter;
    private ActivityMyProductBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMyProductBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        ProductHandler productHandler = new ProductHandler(this);
        adapter = new MyProductAdapter();
        binding.listItemMyProduct.setAdapter(adapter);
        adapter.addProducts(productHandler.getUserProducts(UserContext.LOGGED_USER_ID));
    }

    public void deleteProductByName(String productName) {
        ProductHandler productHandler = new ProductHandler(this);
        productHandler.deleteProduct(productName);
        Toast.makeText(this, "Deleted OK", Toast.LENGTH_SHORT).show();

    }
}