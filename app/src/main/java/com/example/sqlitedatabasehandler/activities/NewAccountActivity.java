package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;

public class NewAccountActivity extends AppCompatActivity {
    EditText firstName, lastName, email, phoneNbr, street, username, password, passConf;
    Button save, cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);
        firstName = (EditText) findViewById(R.id.new_account_first_name);
        lastName = (EditText) findViewById(R.id.new_account_seconde_name);
        email = (EditText) findViewById(R.id.new_account_mail);
        username = (EditText) findViewById(R.id.new_account_username);
        password = (EditText) findViewById(R.id.new_account_password);
        passConf = (EditText) findViewById(R.id.new_account_passwordconf);

        cancel = (Button) findViewById(R.id.new_account_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        save = (Button) findViewById(R.id.new_account_save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserHandler userHandler = new UserHandler(NewAccountActivity.this);
                String fName = firstName.getText().toString();
                String lName = lastName.getText().toString();
                String eMail = email.getText().toString();
                String uName = username.getText().toString();
                String pWord = password.getText().toString();
                // Check if required fields are filled
                if (fName.isEmpty() || lName.isEmpty() || eMail.isEmpty() || uName.isEmpty() || pWord.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Veuillez remplir toutes les cases", Toast.LENGTH_SHORT).show();
                } else {
                    userHandler.addUser(fName, lName, eMail, uName, pWord);
                    Toast.makeText(getApplicationContext(), "Utilisateur créé", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(NewAccountActivity.this, LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}