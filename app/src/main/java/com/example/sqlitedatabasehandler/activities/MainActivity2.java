package com.example.sqlitedatabasehandler.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sqlitedatabasehandler.databaseHandling.DbHandler;
import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;

/**************************************************************************************************/
/************************* OLD HOME SCREEN USED FOR TESTING PURPOSES ******************************/

/**************************************************************************************************/
public class MainActivity2 extends AppCompatActivity {
    Button home;
    Button newAccount;
    Button account;
    Button accountInfo;
    Button login;
    Button deleteAllBtn;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        deleteAllBtn = (Button) findViewById(R.id.deleteAll);
        deleteAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DbHandler dbHandler = new UserHandler(MainActivity2.this);
                dbHandler.DeleteAll();
            }
        });
        login = (Button) findViewById(R.id.go_to_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity2.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        home = (Button) findViewById(R.id.go_to_home);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                intent = new Intent(MainActivity2.this, MainActivity.class);
                startActivity(intent);
            }
        });

        newAccount = (Button) findViewById(R.id.go_to_new_account);
        newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity2.this, NewAccountActivity.class);
                startActivity(intent);
            }
        });
        /* OLD SAVE BUTTON, KEEP FOR SOME FUNCTIONALITIES REUSE MAYBE
        saveBtn = (Button)findViewById(R.id.btnSave);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = firstName.getText().toString()+"\n";
                String location = lastName.getText().toString();
                String designation = email.getText().toString();
                String image = "/res/drawable/" + phoneNbr.getText().toString() + ".png";
                DbHandler dbHandler = new DbHandler(MainActivity.this);
                try {
                    dbHandler.insertUserDetails(username,location,designation, image);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                intent = new Intent(MainActivity.this,DetailsActivity.class);
                startActivity(intent);
                Toast.makeText(getApplicationContext(), "Details Inserted Successfully",Toast.LENGTH_SHORT).show();
            }
        });

         */

        account = (Button) findViewById(R.id.go_to_account);
        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity2.this, AccountActivity.class);
                startActivity(intent);
            }
        });
        accountInfo = (Button) findViewById(R.id.go_to_account_info);
        accountInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity2.this, InfoAccountActivity.class);
                startActivity(intent);
            }
        });
    }
}