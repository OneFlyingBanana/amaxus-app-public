package com.example.sqlitedatabasehandler.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.databaseHandling.UserHandler;
import com.example.sqlitedatabasehandler.exceptions.UserNotFoundException;
import com.example.sqlitedatabasehandler.objects.UserContext;

public class LoginActivity extends AppCompatActivity {

    private EditText eName;
    private EditText ePassword;
    private Button eLogin, newAccount;
    boolean isValid = true;
    private int counter = 5;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String usernamePref = "usernamePref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        eName = findViewById(R.id.username_login);
        ePassword = findViewById(R.id.password_login);
        eLogin = (Button) findViewById(R.id.button_login);

        eLogin.setOnClickListener(view -> {
            String inputName = eName.getText().toString();
            String inputPassword = ePassword.getText().toString();
            // Check if fields are filled
            if (inputName.isEmpty() || inputPassword.isEmpty()) {
                Toast.makeText(LoginActivity.this, "Please enter all the details correctly",
                        Toast.LENGTH_SHORT).show();
            } else {
                isValid = validate(inputName, inputPassword);
                // Check if username and password correspond in database
                if (!isValid) {
                    counter--;
                    Toast.makeText(LoginActivity.this, "Incorrect\n" +
                            "No. of attempts remaining : " + counter, Toast.LENGTH_SHORT).show();
                    // If user failed all attempts, lock button
                    if (counter == 0) {
                        eLogin.setEnabled(false);
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Login succesful ", Toast.LENGTH_SHORT).show();
                    // Store username for session management
                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString(usernamePref, inputName);
                    editor.commit();

                    // Check if user is admin or not
                    if (isAdmin(UserContext.LOGGED_USER_ID)) {
                        Intent intent = new Intent(LoginActivity.this,
                                AdminActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(LoginActivity.this,
                                AccountActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
        newAccount = (Button) findViewById(R.id.new_account_login);
        newAccount.setOnClickListener(view -> {
            Intent intent = new Intent(LoginActivity.this, NewAccountActivity.class);
            startActivity(intent);
        });
    }

    private boolean validate(String name, String password) {
        UserHandler userHandler = new UserHandler(LoginActivity.this);
        try {
            userHandler.checkExistUser(name, password);
            return true;
        } catch (UserNotFoundException ex) {
            return false;
        }
    }

    public boolean isAdmin(int userId) {
        UserHandler userHandler = new UserHandler(LoginActivity.this);
        return userHandler.isAdmin(userId);
    }
}