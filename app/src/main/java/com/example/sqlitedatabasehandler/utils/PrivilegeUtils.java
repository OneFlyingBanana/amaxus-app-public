package com.example.sqlitedatabasehandler.utils;

public class PrivilegeUtils {
    private static final String PRIVILIGE_ADMIN = "Admin";

    public static boolean isPriviligeAdmin(String priviligeCanditate) {
        return priviligeCanditate.equals(PRIVILIGE_ADMIN);
    }
}
