package com.example.sqlitedatabasehandler.databaseHandling;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.sqlitedatabasehandler.objects.Category;
import com.example.sqlitedatabasehandler.objects.Product;
import com.example.sqlitedatabasehandler.objects.ProductUi;

import java.util.ArrayList;
import java.util.List;

public class ProductHandler extends DbHandler {
    public ProductHandler(Context context) {
        super(context);
    }

    // Add one product to Product table
    public void addProduct(String name, int price, int quantity, String detail,
                           String picture, int display, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_PRICE, price);
        contentValues.put(KEY_QUANTITY, quantity);
        contentValues.put(KEY_DETAIL, detail);
        contentValues.put(KEY_PICTURE, picture);
        contentValues.put(KEY_DISPLAY, display);
        contentValues.put(KEY_DATE, date);
        contentValues.put(KEY_ID_PRODUCT_STATUS, 1);
        contentValues.put(KEY_ID_CATEGORY, 1);
        contentValues.put(KEY_ID_USER, 1);
        db.insert(TABLE_Product, null, contentValues);
        db.close();
    }

    // Remove product from table
    public void deleteProduct(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Product, KEY_NAME + " = ?", new String[]{name});
        db.execSQL("delete from product where name = '" + TABLE_Product + "'");
        db.close();
    }


    // Return all products owned by given user
    public List<Product> getUserProducts(int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<Product> products = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + TABLE_Product +
                " where id_user=" + userId, null);
        if (cursor == null) return products;
        cursor.moveToFirst();

        do {
            String name = cursor.getString(cursor.getColumnIndex(KEY_NAME));
            int price = cursor.getInt(cursor.getColumnIndex(KEY_PRICE));
            int quantity = cursor.getInt(cursor.getColumnIndex(KEY_QUANTITY));
            String detail = cursor.getString(cursor.getColumnIndex(KEY_DETAIL));
            String picture = cursor.getString(cursor.getColumnIndex(KEY_PICTURE));
            int display = cursor.getInt(cursor.getColumnIndex(KEY_DISPLAY));
            String date = cursor.getString(cursor.getColumnIndex(KEY_DATE));
            int productStatus = cursor.getInt(cursor.getColumnIndex(KEY_ID_PRODUCT_STATUS));
            int idCategory = cursor.getInt(cursor.getColumnIndex(KEY_ID_CATEGORY));
            int idUser = cursor.getInt(cursor.getColumnIndex(KEY_ID_USER));
            Product product = new Product(name, price, quantity, detail, picture,
                    display, date, productStatus, idCategory, idUser);
            products.add(product);
        } while (cursor.moveToNext());
        db.close();
        return products;
    }

    public List<Category> getCategoryList() {
        SQLiteDatabase db = this.getWritableDatabase();
        List<Category> categoryList = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + TABLE_Category, null);
        if (cursor == null) return categoryList;
        cursor.moveToFirst();
        do {
            String categoryString = cursor.getString(cursor.getColumnIndex(KEY_CATEGORY));
            Category category = new Category(categoryString);
            categoryList.add(category);

        } while (cursor.moveToNext());
        db.close();
        return categoryList;
    }

    // Update information on given product
    public void updateProduct(String name, String price, String quantity, String detail,
                              String picture, String display, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, name);
        //Check to see if values need to be changed (only change if field is not empty)
        if (!price.isEmpty()) {
            int intPrice = Integer.parseInt(price);
            contentValues.put(KEY_PRICE, intPrice);
        }
        if (!quantity.isEmpty()) {
            int intQuantity = Integer.parseInt(quantity);
            contentValues.put(KEY_QUANTITY, intQuantity);
        }
        if (!detail.isEmpty()) {
            contentValues.put(KEY_DETAIL, detail);
        }
        if (!picture.isEmpty()) {
            contentValues.put(KEY_PICTURE, picture);
        }
        if (!display.isEmpty()) {
            int intDisplay = Integer.parseInt(display);
            contentValues.put(KEY_DISPLAY, intDisplay);
        }
        if (!date.isEmpty()) {
            contentValues.put(KEY_DATE, date);
        }
        db.update(TABLE_User, contentValues, KEY_NAME + " = ?", new String[]{name});
    }

    public void addProductStatus(String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_STATUS, status);
        db.insert(TABLE_Product_Status, null, contentValues);
        db.close();
    }

    public void deleteProductStatus(String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Product_Status, KEY_STATUS + " = ?", new String[]{status});
        db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Product_Status + "'");
        db.close();
    }

    public void addComporter(int quantity) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_QUANTITY, quantity);
        db.insert(TABLE_Comporter, null, contentValues);
        db.close();
    }

    public void addCategory(String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CATEGORY, category);
        db.insert(TABLE_Category, null, contentValues);
        db.close();
    }

    public void deleteCategory(String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Category, KEY_CATEGORY + " = ?", new String[]{category});
        db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Category + "'");
        db.close();
    }

    public void modifyCategory(String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_CATEGORY, category);
        db.update(TABLE_Category, contentValues, KEY_CATEGORY + " = ?", new String[]{category});
        db.close();
    }

    public String getCategory(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select " + KEY_ID_PRODUCT + " from " + TABLE_Product + " where " + KEY_NAME + " = " + "'" + name + "'", null);
        cursor.moveToFirst();
        String category = cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME));
        return category;
    }

    // Return all info on products
    public ArrayList<ProductUi> getAllData() {
        ArrayList<ProductUi> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT name, price, detail, status, category FROM product p," +
                " product_status ps, category c WHERE display=1", null);

        while (cursor.moveToNext()) {
            String name = cursor.getString(0);
            int price = cursor.getInt(1);
            String detail = cursor.getString(2);
            String category = cursor.getString(3);
            String status = cursor.getString(4);
            ProductUi product = new ProductUi(name, price, detail, status, category);
            arrayList.add(product);
        }
        return arrayList;
    }

    // Add new product
    public void addProduct(Product product) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, product.getName());
        contentValues.put(KEY_PRICE, product.getPrice());
        contentValues.put(KEY_QUANTITY, product.getQuantity());
        contentValues.put(KEY_DETAIL, product.getDetail());
        contentValues.put(KEY_PICTURE, product.getPicture());
        contentValues.put(KEY_DISPLAY, product.getDisplay());
        contentValues.put(KEY_DATE, product.getDate());
        contentValues.put(KEY_ID_PRODUCT_STATUS, product.getIdProductStatus());
        contentValues.put(KEY_ID_CATEGORY, product.getIdCategory());
        contentValues.put(KEY_ID_USER, product.getIdUser());
        db.insert(TABLE_Product, null, contentValues);
        db.close();
    }
}
