package com.example.sqlitedatabasehandler.databaseHandling;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHandler extends SQLiteOpenHelper {
    // LIST OF ALL TABLE NAMES AND KEYS
    protected static final int DB_VERSION = 1;
    protected static final String DB_NAME = "amaxus_db";
    protected static final String TABLE_Address = "address";
    protected static final String TABLE_Category = "category";
    protected static final String TABLE_Comporter = "comporter";
    protected static final String TABLE_Order = "'order'";
    protected static final String TABLE_Privilege = "privilege";
    protected static final String TABLE_Product = "product";
    protected static final String TABLE_Product_Status = "product_status";
    protected static final String TABLE_Sexe = "sexe";
    protected static final String TABLE_User = "user";

    protected static final String KEY_NAME = "name";
    protected static final String KEY_ID_ADDRESS = "id_addresse";
    protected static final String KEY_POSTAL_CODE = "postal_code";
    protected static final String KEY_CITY = "city";
    protected static final String KEY_ID_CATEGORY = "id_category";
    protected static final String KEY_CATEGORY = "category";
    protected static final String KEY_ID_PRODUCT = "id_product";
    protected static final String KEY_ID_ORDER = "id_order";
    protected static final String KEY_QUANTITY = "quantity";
    protected static final String KEY_ID_USER = "id_user";
    protected static final String KEY_DATE_ORDER = "date_order";
    protected static final String KEY_ID_PRIVILEGE = "id_privilege";
    protected static final String KEY_PRIVILEGE = "privilege";
    protected static final String KEY_ID_PRODUCT_STATUS = "id_product_status";
    protected static final String KEY_PRICE = "price";
    protected static final String KEY_DETAIL = "detail";
    protected static final String KEY_PICTURE = "picture";
    protected static final String KEY_DISPLAY = "display";
    protected static final String KEY_DATE = "date";
    protected static final String KEY_STATUS = "status";
    protected static final String KEY_ID_SEXE = "id_sexe";
    protected static final String KEY_SEXE = "sexe";
    protected static final String KEY_FIRST_NAME = "first_name";
    protected static final String KEY_LAST_NAME = "last_name";
    protected static final String KEY_EMAIL = "email";
    protected static final String KEY_PHONE_NUMBER = "phone_number";
    protected static final String KEY_STREET = "street";
    protected static final String KEY_USER_NAME = "user_name";
    protected static final String KEY_PASSWORD = "password";


    // Strings used for creating and populating database
    protected static String a = "CREATE TABLE address (id_addresse INTEGER PRIMARY KEY AUTOINCREMENT, postal_code INTEGER, city TEXT);";
    protected static String b = "INSERT INTO address (id_addresse, postal_code, city) VALUES (1, 3952, 'Susten');";
    protected static String c = "INSERT INTO address (id_addresse, postal_code, city) VALUES (2, 1936, 'Verbier');";
    protected static String d = "INSERT INTO address (id_addresse, postal_code, city) VALUES (3, 1926, 'Fully');";
    protected static String e = "INSERT INTO address (id_addresse, postal_code, city) VALUES (4, 3960, 'Sierre');";
    protected static String f = "INSERT INTO address (id_addresse, postal_code, city) VALUES (5, 1694, 'Conthey');";
    protected static String g = "INSERT INTO address (id_addresse, postal_code, city) VALUES (6, 1978, 'Lens');";
    protected static String h = "INSERT INTO address (id_addresse, postal_code, city) VALUES (7, 3960, 'Sierre');";
    protected static String i = "INSERT INTO address (id_addresse, postal_code, city) VALUES (8, 3977, 'Granges');";
    protected static String j = "INSERT INTO address (id_addresse, postal_code, city) VALUES (9, 1950, 'Sion');";
    protected static String k = "INSERT INTO address (id_addresse, postal_code, city) VALUES (10, 1976, 'Erde');";
    protected static String l = "INSERT INTO address (id_addresse, postal_code, city) VALUES (11, 1908, 'Riddes');";
    protected static String m = "INSERT INTO address (id_addresse, postal_code, city) VALUES (12, 3970, 'Salgech');";
    protected static String n = "INSERT INTO address (id_addresse, postal_code, city) VALUES (13, 3945, 'Gampel');";
    protected static String o = "INSERT INTO address (id_addresse, postal_code, city) VALUES (14, 1963, 'Vetroz');";


    protected static String p = "CREATE TABLE category(id_category INTEGER PRIMARY KEY AUTOINCREMENT,category TEXT NOT NULL);";
    protected static String q = "INSERT INTO category (id_category, category) VALUES (1, 'Logiciel');";
    protected static String r = "INSERT INTO category (id_category, category) VALUES (2, 'Audio');";
    protected static String s = "INSERT INTO category (id_category, category) VALUES (3, 'PC');";
    protected static String t = "INSERT INTO category (id_category, category) VALUES (4, 'Jeux video');";
    protected static String u = "INSERT INTO category (id_category, category) VALUES (5, 'Reseau');";


    protected static String v = "CREATE TABLE comporter (id_product INTEGER PRIMARY KEY AUTOINCREMENT, id_order INTEGER REFERENCES 'order' (id_order), quantity INTEGER NOT NULL CHECK (QUANTITY BETWEEN 0 AND 100), FOREIGN KEY (id_product) REFERENCES product (id_product));";
    protected static String w = "INSERT INTO comporter (id_product, id_order, quantity) VALUES (2, 1, 1);";


    protected static String x = "CREATE TABLE 'order'(id_order INTEGER PRIMARY KEY AUTOINCREMENT,id_user INTEGER NOT NULL,date_order DATE NOT NULL, FOREIGN KEY (id_user) REFERENCES user (id_user));";
    protected static String y = "INSERT INTO 'order' (id_order, id_user, date_order) VALUES (1, 2, '02.01.2022');";
    protected static String z = "INSERT INTO 'order' (id_order, id_user, date_order) VALUES (2, 2, '02.01.2022');";


    protected static String aa = "CREATE TABLE privilege(id_privilege INTEGER PRIMARY KEY AUTOINCREMENT,privilege TEXT NOT NULL);";
    protected static String ab = "INSERT INTO privilege (id_privilege, privilege) VALUES (1, 'User');";
    protected static String ac = "INSERT INTO privilege (id_privilege, privilege) VALUES (2, 'Admin');";


    protected static String ad = "CREATE TABLE product (id_product INTEGER PRIMARY KEY AUTOINCREMENT, id_category INTEGER NOT NULL, id_product_status INTEGER NOT NULL, id_user INTEGER NOT NULL REFERENCES user (id_user), name TEXT NOT NULL, price INTEGER NOT NULL, detail TEXT NOT NULL, picture TEXT, display INTEGER NOT NULL, date DATE, quantity INTEGER, FOREIGN KEY (id_category) REFERENCES category (id_category), FOREIGN KEY (id_product_status) REFERENCES product_status (id_product_status));";
    protected static String ae = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (1, 3, 3, 2, 'HP', 900, 'Brand new', NULL, 1, '13.12.2021', NULL);";
    protected static String af = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (2, 1, 2, 4, 'Polaroid', 1876, 'Not so bad', NULL, 0, '13.12.2021', NULL);";
    protected static String ag = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (3, 3, 1, 3, 'HP', 100, 'Distroyed', NULL, 1, '12.12.2021', NULL);";
    protected static String ah = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (4, 2, 1, 2, 'HP', 234, 'OK', NULL, 1, '10.12.2021', NULL);";
    protected static String ai = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (5, 2, 1, 4, 'Sony', 123, 'rtet', NULL, 1, '21.12.2021', NULL);";
    protected static String aj = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (6, 3, 1, 4, 'HP', 1800, 'Je n''utilise plus', NULL, 0, '13.12.2021', NULL);";
    protected static String ak = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (7, 1, 3, 2, 'Luni', 50, 'Comme neuf', NULL, 1, '13.12.2021', NULL);";
    protected static String al = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (8, 3, 5, 2, 'TomTom', 35, 'Je n''utilise plus', NULL, 1, '13.12.2021', NULL);";
    protected static String am = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (9, 3, 1, 5, 'GVM', 522, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '01.01.2022', NULL);";
    protected static String an = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (10, 4, 1, 2, 'Sony', 45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '01.01.2022', NULL);";
    protected static String ao = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (11, 2, 5, 1, 'Sony', 15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '01.01.2022', NULL);";
    protected static String ap = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (12, 1, 3, 4, 'Holdit', 89, 'Lorem ipsum dolor sit amet, consectetur adipiscing eli', NULL, 1, '28.12.2021', NULL);";
    protected static String aq = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (13, 3, 4, 2, 'equip', 19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '28.12.2021', NULL);";
    protected static String ar = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (14, 2, 3, 1, 'GVM', 135, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '25.12.2021', NULL);";
    protected static String as = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (15, 1, 2, 5, 'Epos', 99, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '20.12.2021', NULL);";
    protected static String at = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (16, 1, 1, 5, 'Epos', 35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '20.12.2021', NULL);";
    protected static String au = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (17, 4, 2, 5, 'Lenovo', 149, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '20.12.2021', NULL);";
    protected static String av = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (18, 3, 2, 4, 'Lenovo', 2549, 'En double', NULL, 1, '20.12.2021', NULL);";
    protected static String aw = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (19, 4, 2, 4, 'Bomann', 150, 'Je n''utilise plus', NULL, 1, '20.12.2021', NULL);";
    protected static String ax = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (20, 4, 2, 4, 'AMD', 120, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String ay = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (21, 3, 1, 1, 'Lenovo', 23, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String az = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (22, 3, 5, 2, 'Lenovo', 45, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String ba = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (23, 5, 1, 3, 'AMD', 60, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String bb = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (24, 3, 1, 4, 'Lenovo', 340, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String bc = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (25, 5, 4, 5, 'Sony', 199, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String bd = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (26, 5, 1, 2, 'AMD', 680, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String be = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (27, 3, 2, 3, 'Lexmark', 1999, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String bf = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (28, 5, 3, 4, 'AMD', 20, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";
    protected static String bg = "INSERT INTO product (id_product, id_category, id_product_status, id_user, name, price, detail, picture, display, date, quantity) VALUES (29, 3, 2, 5, 'Lexmark', 10, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', NULL, 1, '10.01.2022', NULL);";


    protected static String bh = "CREATE TABLE product_status(id_product_status INTEGER PRIMARY KEY AUTOINCREMENT,status TEXT NOT NULL);";
    protected static String bi = "INSERT INTO product_status (id_product_status, status) VALUES (1, 'Très bon état');";
    protected static String bj = "INSERT INTO product_status (id_product_status, status) VALUES (2, 'Bon état');";
    protected static String bk = "INSERT INTO product_status (id_product_status, status) VALUES (3, 'Moyen');";
    protected static String bl = "INSERT INTO product_status (id_product_status, status) VALUES (4, 'Mauvais etat');";
    protected static String bm = "INSERT INTO product_status (id_product_status, status) VALUES (5, 'Très bon état');";


    protected static String bn = "CREATE TABLE sexe(id_sexe INTEGER PRIMARY KEY AUTOINCREMENT,sexe VARCHAR(20) NOT NULL);";
    protected static String bo = "INSERT INTO sexe (id_sexe, sexe) VALUES (1, 'Man');";
    protected static String bp = "INSERT INTO sexe (id_sexe, sexe) VALUES (2, 'Woman');";


    protected static String bq = "CREATE TABLE stock (id INTEGER PRIMARY KEY AUTOINCREMENT, id_product INT REFERENCES product (id_product), quantityInStock INT NOT NULL, quantityOrdered INT NOT NULL, expectedDelivery DATE);";
    protected static String br = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (1, 4, 0, 0, NULL);";
    protected static String bs = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (2, 5, 0, 0, NULL);";
    protected static String bt = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (3, 6, 0, 0, NULL);";
    protected static String bu = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (4, 7, 0, 0, NULL);";
    protected static String bv = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (5, 8, 0, 0, NULL);";
    protected static String bw = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (6, 9, 0, 0, NULL);";
    protected static String bx = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (7, 10, 0, 0, NULL);";
    protected static String by = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (8, 11, 0, 0, NULL);";
    protected static String bz = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (9, 12, 0, 0, NULL);";
    protected static String ca = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (10, 13, 0, 0, NULL);";
    protected static String cb = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (11, 14, 0, 0, NULL);";
    protected static String cc = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (12, 15, 0, 0, NULL);";
    protected static String cd = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (13, 16, 0, 0, NULL);";
    protected static String ce = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (14, 17, 0, 0, NULL);";
    protected static String cf = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (15, 18, 0, 0, NULL);";
    protected static String cg = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (16, 19, 0, 0, NULL);";
    protected static String ch = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (17, 20, 0, 0, NULL);";
    protected static String ci = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (18, 21, 0, 0, NULL);";
    protected static String cj = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (19, 22, 0, 0, NULL);";
    protected static String ck = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (20, 23, 0, 0, NULL);";
    protected static String cl = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (21, 24, 0, 0, NULL);";
    protected static String cm = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (22, 25, 0, 0, NULL);";
    protected static String cn = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (23, 26, 0, 0, NULL);";
    protected static String co = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (24, 27, 0, 0, NULL);";
    protected static String cp = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (25, 28, 0, 0, NULL);";
    protected static String cq = "INSERT INTO stock (id, id_product, quantityInStock, quantityOrdered, expectedDelivery) VALUES (26, 29, 0, 0, NULL);";


    protected static String cr = "CREATE TABLE user (id_user INTEGER PRIMARY KEY AUTOINCREMENT, id_addresse INTEGER, id_privilege INTEGER NOT NULL, id_sexe TEXT, first_name TEXT NOT NULL, last_name TEXT NOT NULL, email TEXT NOT NULL, phone_number INTEGER, street TEXT, picture TEXT, user_name TEXT NOT NULL, password TEXT NOT NULL, FOREIGN KEY (id_addresse) REFERENCES address (id_addresse), FOREIGN KEY (id_sexe) REFERENCES sexe (id_sexe), FOREIGN KEY (id_privilege) REFERENCES privilege (id_privilege));";
    protected static String cs = "INSERT INTO user (id_user, id_addresse, id_privilege, id_sexe, first_name, last_name, email, phone_number, street, picture, user_name, password) VALUES (1, 1, 1, '1', 'Koko', 'koko', 'koko', 'kkk', '', '', 'koko', 'koko');";
    protected static String ct = "INSERT INTO user (id_user, id_addresse, id_privilege, id_sexe, first_name, last_name, email, phone_number, street, picture, user_name, password) VALUES (2, 5, 2, '2', 'Joanna', 'Baranowska', 'j.baranowska@hotmail.com', 123456, NULL, NULL, 'Joanna', 'Joanna');";
    protected static String cu = "INSERT INTO user (id_user, id_addresse, id_privilege, id_sexe, first_name, last_name, email, phone_number, street, picture, user_name, password) VALUES (3, 1, 1, '1', 'Rambo', 'Bambo', 'rambo@bambo', NULL, NULL, NULL, 'Rambo', 'Rambo');";
    protected static String cv = "INSERT INTO user (id_user, id_addresse, id_privilege, id_sexe, first_name, last_name, email, phone_number, street, picture, user_name, password) VALUES (4, 3, 2, '1', 'Maxime', 'Denervaud', 'max@den', 123456, NULL, NULL, 'Max', 'Max');";
    protected static String cw = "INSERT INTO user (id_user, id_addresse, id_privilege, id_sexe, first_name, last_name, email, phone_number, street, picture, user_name, password) VALUES (5, 2, 2, '1', 'Roald', 'Brunell', 'roald@brunell', 123456, NULL, NULL, 'Roald', 'Roald');";

    public DbHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Creation of tables
    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(a);
            db.execSQL(b);
            db.execSQL(c);
            db.execSQL(d);
            db.execSQL(e);
            db.execSQL(f);
            db.execSQL(g);
            db.execSQL(h);
            db.execSQL(i);
            db.execSQL(j);
            db.execSQL(k);
            db.execSQL(l);
            db.execSQL(m);
            db.execSQL(n);
            db.execSQL(o);
            db.execSQL(p);
            db.execSQL(q);
            db.execSQL(r);
            db.execSQL(s);
            db.execSQL(t);
            db.execSQL(u);
            db.execSQL(v);
            db.execSQL(w);
            db.execSQL(x);
            db.execSQL(y);
            db.execSQL(z);
            db.execSQL(aa);
            db.execSQL(ab);
            db.execSQL(ac);
            db.execSQL(ad);
            db.execSQL(ae);
            db.execSQL(af);
            db.execSQL(ag);
            db.execSQL(ah);
            db.execSQL(ai);
            db.execSQL(aj);
            db.execSQL(ak);
            db.execSQL(al);
            db.execSQL(am);
            db.execSQL(an);
            db.execSQL(ao);
            db.execSQL(ap);
            db.execSQL(aq);
            db.execSQL(ar);
            db.execSQL(as);
            db.execSQL(at);
            db.execSQL(au);
            db.execSQL(av);
            db.execSQL(aw);
            db.execSQL(ax);
            db.execSQL(ay);
            db.execSQL(az);
            db.execSQL(ba);
            db.execSQL(bb);
            db.execSQL(bc);
            db.execSQL(bd);
            db.execSQL(be);
            db.execSQL(bf);
            db.execSQL(bg);
            db.execSQL(bh);
            db.execSQL(bi);
            db.execSQL(bj);
            db.execSQL(bk);
            db.execSQL(bl);
            db.execSQL(bm);
            db.execSQL(bn);
            db.execSQL(bo);
            db.execSQL(bp);
            db.execSQL(bq);
            db.execSQL(br);
            db.execSQL(bs);
            db.execSQL(bt);
            db.execSQL(bu);
            db.execSQL(bv);
            db.execSQL(bw);
            db.execSQL(bx);
            db.execSQL(by);
            db.execSQL(bz);
            db.execSQL(ca);
            db.execSQL(cb);
            db.execSQL(cc);
            db.execSQL(cd);
            db.execSQL(ce);
            db.execSQL(cf);
            db.execSQL(cg);
            db.execSQL(ch);
            db.execSQL(ci);
            db.execSQL(cj);
            db.execSQL(ck);
            db.execSQL(cl);
            db.execSQL(cm);
            db.execSQL(cn);
            db.execSQL(co);
            db.execSQL(cp);
            db.execSQL(cq);
            db.execSQL(cr);
            db.execSQL(cs);
            db.execSQL(ct);
            db.execSQL(cu);
            db.execSQL(cv);
            db.execSQL(cw);
    }

    // Recreate database on upgrade
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if exist
        db.beginTransaction();
        try {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Category);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Address);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Comporter);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Order);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Privilege);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Product);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Product_Status);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_Sexe);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_User);
            // Create tables again
            onCreate(db);
        } finally {
            db.endTransaction();
        }
    }

    // Empty one table by name
    public void deleteTable(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("delete from " + table);
            db.execSQL("delete from sqlite_sequence where name = '" + table + "'");
            db.close();
        } finally {
            db.endTransaction();
        }
    }

    // Delete all content from all tables and resets auto-incremented id
    public void DeleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("delete from " + TABLE_User);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_User + "'");
            db.execSQL("delete from " + TABLE_Address);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Address + "'");
            db.execSQL("delete from " + TABLE_Category);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Category + "'");
            db.execSQL("delete from " + TABLE_Comporter);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Comporter + "'");
            db.execSQL("delete from " + TABLE_Order);
            db.execSQL("delete from sqlite_sequence where name = " + TABLE_Order + "");
            db.execSQL("delete from " + TABLE_Privilege);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Privilege + "'");
            db.execSQL("delete from " + TABLE_Product);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Product + "'");
            db.execSQL("delete from " + TABLE_Product_Status);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Product_Status + "'");
            db.execSQL("delete from " + TABLE_Sexe);
            db.execSQL("delete from sqlite_sequence where name = '" + TABLE_Sexe + "'");
        } finally {
            db.endTransaction();
        }
    }
}