package com.example.sqlitedatabasehandler.databaseHandling;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.sqlitedatabasehandler.exceptions.UserNotFoundException;
import com.example.sqlitedatabasehandler.objects.UserContext;
import com.example.sqlitedatabasehandler.utils.PrivilegeUtils;

public class UserHandler extends DbHandler {
    public UserHandler(Context context) {
        super(context);
    }

    // Get First Name from username
    public String getUserFirstName(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select " + KEY_FIRST_NAME + " from " + TABLE_User + " where " + KEY_USER_NAME + " = " + "'" + username + "'", null);
        cursor.moveToFirst();
        String firstName = cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME));
        return firstName;
    }

    // Get Last Name from username
    public String getUserLastName(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select " + KEY_LAST_NAME + " from " + TABLE_User + " where " + KEY_USER_NAME + " = " + "'" + username + "'", null);
        cursor.moveToFirst();
        String lastName = cursor.getString(cursor.getColumnIndex(KEY_LAST_NAME));
        return lastName;
    }

    // Add user to databse
    public void addUser(String firstName, String lastName, String email, String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FIRST_NAME, firstName);
        contentValues.put(KEY_LAST_NAME, lastName);
        contentValues.put(KEY_EMAIL, email);
        contentValues.put(KEY_USER_NAME, username);
        contentValues.put(KEY_PASSWORD, password);
        contentValues.put(KEY_ID_PRIVILEGE, "1");
        db.insert(TABLE_User, null, contentValues);
        db.close();
    }

    // Update User Details
    public void updateUser(String firstName, String lastName, String email, String phoneNbr, String street, String username, String password) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FIRST_NAME, firstName);
        //Check to see if values need to be changed (only change if field is not empty)
        if (!lastName.isEmpty()) {
            contentValues.put(KEY_LAST_NAME, lastName);
        }
        if (!email.isEmpty()) {
            contentValues.put(KEY_EMAIL, email);
        }
        if (!username.isEmpty()) {
            contentValues.put(KEY_USER_NAME, username);
        }
        if (!password.isEmpty()) {
            contentValues.put(KEY_PASSWORD, password);
        }
        if (!phoneNbr.isEmpty()) {
            contentValues.put(KEY_PHONE_NUMBER, phoneNbr);
        }
        if (!street.isEmpty()) {
            contentValues.put(KEY_STREET, street);
        }
        db.update(TABLE_User, contentValues, KEY_FIRST_NAME + " = ?", new String[]{firstName});
    }

    // Delete User by first name
    public void deleteUser(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_User, KEY_FIRST_NAME + " = ?", new String[]{name});
        db.execSQL("delete from sqlite_sequence where name = '" + TABLE_User + "'");
        db.close();
    }

    // Stores image in array of bytes in database
    public void updateImage(String username, byte[] img) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PICTURE, img);
        db.update(TABLE_User, contentValues, KEY_USER_NAME + " = ?", new String[]{username});
        db.close();
    }

    // Gets image as array of bytes from database and turns it into a bitmap before returning
    public Bitmap getImage(String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select " + KEY_PICTURE + " from " + TABLE_User + " where " + KEY_USER_NAME + " = ?", new String[]{username});
        cursor.moveToFirst();
        byte[] bitmap = cursor.getBlob(0);
        if (bitmap == null) {
            return null;
        }
        Bitmap image = BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
        return image;
        /************ TESTING -> BREAKING QUERY IN 2 TO ALLOW BIGGER FILES TO BE LOADED OUT -> CURSOR MAX SIZE TOO SMALL FOR SOME IMAGES ***************************
         Cursor cursor1 = db.rawQuery("select substr(" + KEY_PICTURE + ", 1, 1000000) from " + TABLE_User + " where " + KEY_USER_NAME + " = ?", new String[]{username});
         cursor1.moveToFirst();
         Cursor cursor2 = db.rawQuery("select substr(" + KEY_PICTURE + ", 1000001, 1000000) from " + TABLE_User + " where " + KEY_USER_NAME + " = ?", new String[]{username});
         cursor.moveToFirst();
         ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
         byte[] bitmap1 = cursor1.getBlob(0);
         byte[] bitmap = cursor.getBlob(0);
         outputStream.write( bitmap );
         outputStream.write( bitmap1);
         byte[] bitmap2 = outputStream.toByteArray();
         ***************************************************************************************************************************************************/
    }

    // Cheek if user already exists in database
    public void checkExistUser(String userName, String password) throws UserNotFoundException {
        SQLiteDatabase db = this.getReadableDatabase();
        UserContext.USERNAME = userName;
        UserContext.PASSWORD = password;
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE user_name = " +
                "'" + userName + "'" + " AND password = " + "'" + password + "'", null);
        if (cursor == null) {
            throw new UserNotFoundException();
        }
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            UserContext.LOGGED_USER_ID = cursor.getInt(0);
        }
        if (cursor.getCount() == 0) {
            throw new UserNotFoundException();
        }
    }

    // Check if user has administrator privileges
    public boolean isAdmin(int userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM user WHERE id_user =" + userId,
                null);
        cursor.moveToFirst();
        int privilegeId = cursor.getInt(cursor.getColumnIndex(KEY_ID_PRIVILEGE));

        Cursor cursor2 = db.rawQuery("SELECT * FROM privilege WHERE id_privilege =" +
                privilegeId, null);
        cursor2.moveToFirst();
        String privilege = cursor2.getString(cursor2.getColumnIndex(KEY_PRIVILEGE));
        return PrivilegeUtils.isPriviligeAdmin(privilege);
    }
}