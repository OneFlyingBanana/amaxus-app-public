package com.example.sqlitedatabasehandler.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sqlitedatabasehandler.R;
import com.example.sqlitedatabasehandler.objects.ProductUi;

import java.util.ArrayList;


public class MyAdapter extends BaseAdapter {

    Context context;
    ArrayList<ProductUi> arrayList;

    public MyAdapter(Context context, ArrayList<ProductUi> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public long getItemId(int position) {
        return 0;
    }

    public Object getItem(int position) {
        return arrayList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_product_home, null);

        //ImageView imageProduct = (ImageView)convertView.findViewById(R.id.image_product_home);
        TextView name = (TextView) convertView.findViewById(R.id.name_product_home);
        TextView price = (TextView) convertView.findViewById(R.id.price_product_home);
        TextView category = (TextView) convertView.findViewById(R.id.category_product_home);
        TextView status = (TextView) convertView.findViewById(R.id.status_product_home);
        TextView detail = (TextView) convertView.findViewById(R.id.detail_product_home);

        ProductUi product = arrayList.get(position);

        //imageProduct.setImageURI(product.getImageUrl());
        name.setText(product.getName());
        price.setText(String.valueOf(product.getPrice()));
        category.setText(product.getCategory());
        status.setText(product.getStatus());
        detail.setText(product.getDetail());

        //https://www.youtube.com/watch?v=6q4-Ge0UMKY
        return convertView;
    }

    public int getCount() {
        return this.arrayList.size();
    }
}
