package com.example.sqlitedatabasehandler.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sqlitedatabasehandler.activities.MyProductsActivity;
import com.example.sqlitedatabasehandler.databinding.ItemMyProductBinding;
import com.example.sqlitedatabasehandler.objects.Product;

import java.util.ArrayList;
import java.util.List;

public class MyProductAdapter extends RecyclerView.Adapter<MyProductAdapter.ProductViewHolder> {
    private final List<Product> products = new ArrayList<>();
    private MyProductsActivity myProductsActivity;

    public void addProducts(List<Product> products) {
        this.products.clear();
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        myProductsActivity = (MyProductsActivity) parent.getContext();  // delete func
        return new ProductViewHolder(ItemMyProductBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.binding.myproductProductName.setText(product.getName());
        holder.binding.myproductProductPrice.setText(String.valueOf(product.getPrice()));
        holder.binding.myproductProductDeleteButton.setOnClickListener(view -> {
            myProductsActivity.deleteProductByName(product.getName());
            products.remove(position);  // products list [0,1,2.....]
            notifyDataSetChanged();
        });   // delete function
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        private ItemMyProductBinding binding;

        public ProductViewHolder(@NonNull ItemMyProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
