package com.example.sqlitedatabasehandler.objects;

public class Product {
    private final String name;
    private final int price;
    private final int quantity;
    private final String detail;
    private final String picture;
    private final int display;
    private final String date;
    private final int idProductStatus;
    private final int idCategory;
    private final int idUser;

    public Product(String name, int price, int quantity, String detail, String picture, int display, String date, int idProductStatus, int idCategory, int idUser) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.detail = detail;
        this.picture = picture;
        this.display = display;
        this.date = date;
        this.idProductStatus = idProductStatus;
        this.idCategory = idCategory;
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDetail() {
        return detail;
    }

    public String getPicture() {
        return picture;
    }

    public int getDisplay() {
        return display;
    }

    public String getDate() {
        return date;
    }

    public int getIdProductStatus() {
        return idProductStatus;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public int getIdUser() {
        return idUser;
    }
}
