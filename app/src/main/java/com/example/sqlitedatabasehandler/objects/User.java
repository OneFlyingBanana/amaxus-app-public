package com.example.sqlitedatabasehandler.objects;

public class User {

    private String firstName;
    private String lastName;
    private String email;
    private String street;
    private String avatarUrl;
    private String telephone;

    public User(String firstName, String lastName, String email, String street, String avatarUrl, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.street = street;
        this.avatarUrl = avatarUrl;
        this.telephone = telephone;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getStreet() {
        return street;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getTelephone() {
        return telephone;
    }
}