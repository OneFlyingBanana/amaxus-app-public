package com.example.sqlitedatabasehandler.objects;

public class ProductUi {

    private String name;
    private int price;
    private int quantity;
    private String detail;
    private String category;
    private String status;
    private String imageUrl;
    private int display;

    public ProductUi(String name, int price, String detail, String category, String status){
        this.name = name;
        this.price = price;
        this.detail = detail;
        this.category = category;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDetail() {
        return detail;
    }

    public String getCategory() {
        return category;
    }

    public String getStatus() {
        return status;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getDisplay() {
        return display;
    }
}
