package com.example.sqlitedatabasehandler.objects;

public class Category {
    private final String category;

    public Category(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }
}
