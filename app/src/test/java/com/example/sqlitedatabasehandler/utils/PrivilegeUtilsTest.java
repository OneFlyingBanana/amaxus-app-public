package com.example.sqlitedatabasehandler.utils;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrivilegeUtilsTest {
    @Test
    public void shouldReturnTrueBecauseUserIsAdmin() {
        String privilegeInput = "Admin";
        boolean actual = PrivilegeUtils.isPriviligeAdmin(privilegeInput);
        assertTrue(actual);
    }

    @Test
    public void shouldReturnFalseBecauseUserIsNotAdmin() {
        String privilegeInput = "User";
        boolean actual = PrivilegeUtils.isPriviligeAdmin(privilegeInput);
        assertFalse(actual);
    }

}