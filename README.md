# Amaxus App

Projet de fin du semestre d'automne/hiver 2021 pour la DTA. Ce projet est le fruit du travail accompli par Joanna Baranowska, Maxime Dénervaud et Roald Brunell. Après 10 jours de travail dédiés à ce projet nous vous invitons à tester et explorer notre application :)


***


## Description
Ce projet est une application simulant un magasin en ligne "Amaxus". Cette application propose les fonctionnalités suivantes :
    - Parcourir la liste des produits disponibles
    - Créer un compte utilisateur
    - S'enregistrer en tant qu'utilisateur ou administrateur
    - Modifer ses données personnelles et choisir une photo de profil
    - Voir ses annonces personnelles
    - Ajouter et supprimer une annonce


## Installation
Pour tester l'application nous vous invitons à installer notre app sur votre appareil android (1) ou à télécharger un émulateur android (2) sur lequel vous pourrez télécharger puis installer notre app :

(1) Vous pouvez télécharger le fichier "app-debug.apk" situé dans le dossier "APK" ci-dessus ou via le lien suivant : https://filesender.switch.ch/filesender2/?s=download&token=d512d102-7dbd-415a-82d2-1c80f59d0147. Ce fichier, une fois ouvert depuis votre téléphone android va installer notre app sur votre appareil.

(2) Il vous faut d'abord un émulateur capable d'installer notre application. Nous avons testé le bon fonctionnement sur l'émulateur fourni par Android Studio. Vous pouvez le télécharger au lien suivant : https://developer.android.com/studio.

Une fois téléchargé vous pouvez créer un nouveau projet à partir d'une Version Control (VCS). Il vous sera demandé un URL afin de pouvoir cloner notre projet. Entrez "https://gitlab.com/OneFlyingBanana/amaxus-app-public" puis clickez sur "Clone". 

Une fois le projet téléchargé cliquez sur "Trust Project" et attendez la fin de la construction automatique du projet.
Avant de pouvoir exécuter le projet il vous faut choisir un émulateur à installer. Au sommet de votre fenêtre cliquez sur AVD Manager 

![image-1.png](./image-1.png)

Sur la nouvelle fenêtre qui s'ouvre cliquez sur "Create Virtual Device" puis choisissez un téléphone. Nous recommandons le Pixel 2, émulateur que nous avons utilisé et testé avec l'application.
Cliquez sur "Next" puis choisissez l'image "R" et téléchargez la.

![image-2.png](./image-2.png)

Une fois terminé cliquez sur "Next" puis enfin sur "Finish"

Votre émulateur et projet sont prêts! Pour tester l'application cliquez sur le petit triangle vert au somment de votre écran.

![image.png](./image.png)

L'application et votre émulateur vont démarrer et vous pourrez tester notre application :)



## Usage
Ce projet est en libre accès


## Statut du projet
Ceci est la version finale du projet à rendre, mais il n'est tout de fois pas exclu que nous revenions y travailler dans le futur


## Prototype
Pour consulter le prototype de notre projet vous pouvez consulter le lien suivant : https://www.figma.com/proto/2C9yCDIcrqb8q3DT5WBLtw/DTA_projet_ev_s1?node-id=2%3A5&starting-point-node-id=2%3A5

